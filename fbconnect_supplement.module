<?php

function fbconnect_supplement_custom_theme() {
  return array(
    'custom_fbconnect_button' => array(
      'arguments' => array('type' => 'ask', 'text' => t('Connect'), 'desc' => t('Sign in using Facebook')),
    ),
  );
}

function theme_custom_fbconnect_button($type, $text, $desc) {
  $form = array();
  fbconnect_supplement_add_fbconnect($type, $text, $desc, $form);
  return drupal_render($form);
}

function fbconnect_supplement_add_fbconnect($type, $text, $desc, &$form) {
  $attr = array( "text" => $text );

  if ($type == 'register') {
    $value = fbconnect_render_register_button($attr);
  }
  else if ($type == 'login') {
    $value = fbconnect_render_login_button($attr);
  }
  else {
    $value = fbconnect_render_button($attr);
  }

  $form['fbconnect_supplement_button'] = array(
    '#type' => 'item',
    '#description' => $desc,
    '#value' => $value,
    '#weight' => -20,
    '#id' => 'fbconnect_supplement_button',
    '#suffix' => '<hr />',
  );
}

/**
 * Implementation of hook_fbconnect_user_data().
 */
function fbconnect_supplement_fbconnect_user_data($data) {
  global $user;

  $content_profile_type = 'profile'; 
  $content_profile = content_profile_load($content_profile_type, $user->uid, '', TRUE);
  if ($content_profile->nid > 0) {

    node_prepare($content_profile);
    $content_profile->field_first_name[0]['value'] = $data['first_name'];
    $content_profile->field_last_name[0]['value'] = $data['last_name'];
    node_save($content_profile);
    fbconnect_supplement_update_user_picture(user_load($user->uid), $content_profile);
  }
}

function _fbconnect_supplement_check_fb_picture($content_profile) {
    // Check if filename contains an fb path
    $filename = $content_profile->field_picture[0]['filename'];
    $filepath = $content_profile->field_picture[0]['filepath'];
    $t = explode('/', $filepath);
    if (count($t) < 3) {
      return FALSE;
    }
    $t = $t[count($t) - 2];

    if (strstr($content_profile->field_picture[0]['filename'], 'picture-fb_') != '' && $t == 'fb-images') {
      return TRUE;
    }

    return FALSE;
}

function fbconnect_supplement_update_user_picture($account = NULL, $content_profile = NULL) {
  global $user;
  if ($account == NULL) {
    $account = $user;
  }
  $use_fb_picture = $account->fb_avatar;
  $fb_picture = $account->fb_image;
  $fbuid = $account->fbuid;

  if ($content_profile == NULL) {
    $content_profile_type = 'profile'; 
    $content_profile = content_profile_load($content_profile_type, $account->uid);
  }

  if ($content_profile->nid <= 0) {
    return FALSE;
  }

  if ($use_fb_picture) {
    if (!$content_profile) {
      drupal_set_message(t('Your user picture could not be updated, because your profile could not be found.'), 'error');
      return FALSE;
    }
    module_load_include('inc', 'content', 'includes/content.node_form');
    $profile_field = content_fields('field_picture', $content_profile_type);
    if (!$profile_field) {
      return FALSE;
    }

    $result = drupal_http_request($fb_picture);
    if ($result->code != 200 && $result->code != 302) {
      drupal_set_message(t('Your user picture could not be retrieved from Facebook.'), 'error');
      return FALSE;
    }

    $filename = 'picture-fb_'. $fbuid .'.jpg';
    $dest = file_directory_temp();
    $dest .= '/'. $filename;
    $filepath = file_save_data($result->data, $dest, FILE_EXISTS_RENAME);

    $destpath = filefield_widget_file_path($profile_field) . 'fb-images/';
    file_check_directory($destpath, FILE_CREATE_DIRECTORY);
    $destpath .= '/';

    $file = field_file_save_file($filepath, array(), $destpath);
    $file = field_file_save($content_profile, $file);

    $content_profile->field_picture[0] = $file;

    node_save($content_profile);

    @unlink($filepath);

    drupal_set_message(t('Your user picture was successfully retrieved from Facebook.'));
  } else {

    if (!$content_profile->nid > 0) {
      drupal_set_message(t('Your user picture could not be updated, because your profile could not be found.'), 'error');
      return FALSE;
    }

    if (_fbconnect_supplement_check_fb_picture($content_profile)) {
      unset($content_profile->field_picture[0]);
      node_save($content_profile);
      drupal_set_message(t('Your user picture from Facebook was removed.'));
    }
  }
}

/**
 * Implementation of hook_user().
 */
function fbconnect_supplement_user($op, &$edit, &$account, $category = NULL) {

  switch($op) {
    case 'after_update':

      if (isset($edit['fb_avatar'])) {
        fbconnect_supplement_update_user_picture($account);
      }
      break;
  }
}

/**
 * Implementation of hook_fbconnect_user_unregistered()
 */
function fbconnect_supplement_fbconnect_user_unregistered($account) {
  $account->fb_avatar = 0;
  fbconnect_supplement_update_user_picture($account);
}

function fbconnect_supplement_update_user_picture_submit($form, &$form_state) {
  $node = $form['#node'];
  fbconnect_supplement_update_user_picture(user_load($node->uid), $node);
}

function fbconnect_supplement_import_user_picture_submit($form, &$form_state) {
  $node = $form['#node'];
  $account = user_load($node->uid);

  $data = array();
  $data['fb_avatar'] = 1;

  user_save($account, $data);
}

/**
 * Implementation of hook_form_alter().
 */
function fbconnect_supplement_form_alter(&$form, &$form_state, $form_id) {

  if (isset($form['type']) && isset($form['#node']) && $form['type']['#value'] .'_node_form' == $form_id) {
    if (is_content_profile($form['type']['#value'])) {
      $account = user_load($form['#node']->uid);
      if (_fbconnect_supplement_check_fb_picture($form['#node'])) {
        $form['update_user_picture'] = array(
          '#type' => 'submit',
          '#value' => t('Update Facebook user picture'),
          '#suffix' => '<div class="update-user-picture-description">' . t('Your user picture was automatically imported from Facebook. Click here to update it.') . '</div>',
          '#submit' => array('fbconnect_supplement_update_user_picture_submit'),
        );
      } else if ($account->fb_image != '') {
        $form['import_user_picture'] = array(
          '#type' => 'submit',
          '#value' => t('Import user picture from Facebook'),
          '#suffix' => '<div class="update-user-picture-description">' . t('Importing your user picture from Facebook will overwrite your currently set picture.') . '</div>',
          '#submit' => array('fbconnect_supplement_import_user_picture_submit'),
        );
      }
    }
  }

  if ($form_id == 'user_login') {
    fbconnect_supplement_add_fbconnect('login', t('Login via Facebook'), t('Login using Facebook'), $form);
  }

  if ($form_id == 'user_register') {
    fbconnect_supplement_add_fbconnect('register', t('Quick-Register via Facebook'), t('Register using Facebook'), $form);
    $form['#submit'][] = 'fbconnect_supplement_user_register_authenticate';
  }

  if($form_id == 'user_profile_form') {
    unset($form['picture']);
  }

  if ($form_id == 'fbconnect_user_settings_form') {
    if (!empty($form['account']['#value'])) {
      $account = $form['account']['#value'];
      $content_profile_type = 'profile'; 
      $content_profile = content_profile_load($content_profile_type, $account->uid);
      $form['fb_avatar']['#default_value'] = _fbconnect_supplement_check_fb_picture($content_profile)?1:0;
      $form['fb_avatar']['#title'] = t('Import my Facebook picture as user picture');
      $form['fb_avatar']['#description'] = t('This will overwrite your currently set picture.');
    }
  }

}

/**
 * Authenticate registered user
 *
 * @param $form
 * @param $form_state
 */
function fbconnect_supplement_user_register_authenticate($form, &$form_state) {
  global $user;

  if (!isset($_REQUEST['destination']) && $user->uid > 0) {

    drupal_set_message('Welcome! Please take a moment to complete your profile.');
    $_REQUEST['destination'] = 'user/' . $user->uid . '/edit/profile';

  }

}
